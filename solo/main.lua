function love.load()
  --package.path = package.path .. "C:/Users/t016021f/Downloads/seven"
  --require("loveui/main.lua")
	bg = love.graphics.newImage("sprites/office.png")
  click = love.graphics.newImage("sprites/ClickCase.png")
  clicktext = love.graphics.newImage("sprites/click.png")
  click_auto_1 = love.graphics.newImage("sprites/tempautoclick1.png")
  click_auto_2 = love.graphics.newImage("sprites/tempautoclick2.png")
  click_auto_3 = love.graphics.newImage("sprites/tempautoclick3.png")
  click_auto_4 = love.graphics.newImage("sprites/tempautoclick4.png")
  
  buy_1= love.graphics.newImage("sprites/buy1.png")
  buy_10= love.graphics.newImage("sprites/buy10.png")
  buy_1_lit= love.graphics.newImage("sprites/buy1_lit.png")
  buy_10_lit= love.graphics.newImage("sprites/buy10_lit.png")
  
  cases = 0
  qt = 0
  x = 0
  y = 0
  currently_buying = 1
  
  click_auto_1_rank = 0
  click_auto_1_basecost = 25
  click_auto_1_running = false
  click_auto_1_cost = 0
  click_auto_1_increase = 1
  
  click_auto_2_rank = 0
  click_auto_2_basecost = 100
  click_auto_2_running = false
  click_auto_2_cost = 0
  click_auto_2_increase = 5
  
  click_auto_3_rank = 0
  click_auto_3_basecost = 250
  click_auto_3_running = false
  click_auto_3_cost = 0
  click_auto_3_increase = 25
  
  click_auto_4_rank = 0
  click_auto_4_basecost = 1000
  click_auto_4_running = false
  click_auto_4_cost = 0
  click_auto_4_increase = 100

end


--[[
function love.mouse.isDown(button) 
  if (button == 1) then
    caseClick()
   end
end
]]--
function love.update(dt)
  --qt represents the timer that will be reset every second
qt = qt + dt
-- gets the x and y position of the mouse
x, y = love.mouse.getPosition()

--Updating costs
if (currently_buying == 1) then
  click_auto_1_cost = math.floor(click_auto_1_basecost * 1.15^(click_auto_1_rank))
  click_auto_2_cost = math.floor(click_auto_2_basecost * 1.15^(click_auto_2_rank))
  click_auto_3_cost = math.floor(click_auto_3_basecost * 1.15^(click_auto_3_rank))
  click_auto_4_cost = math.floor(click_auto_4_basecost * 1.15^(click_auto_4_rank))
else 
  click_auto_1_cost = math.floor(click_auto_1_basecost * 1.15^(click_auto_1_rank)) 
  click_auto_2_cost = math.floor(click_auto_2_basecost * 1.15^(click_auto_2_rank))
  click_auto_3_cost = math.floor(click_auto_3_basecost * 1.15^(click_auto_3_rank))
  click_auto_4_cost = math.floor(click_auto_4_basecost * 1.15^(click_auto_4_rank))
  
for i =2,currently_buying do
  click_auto_1_cost = click_auto_1_cost + math.floor(click_auto_1_basecost * 1.15^(click_auto_1_rank+i)) 
  click_auto_2_cost = click_auto_1_cost +math.floor(click_auto_2_basecost * 1.15^(click_auto_2_rank+i))
  click_auto_3_cost = click_auto_1_cost +math.floor(click_auto_3_basecost * 1.15^(click_auto_3_rank+i))
  click_auto_4_cost = click_auto_1_cost +math.floor(click_auto_4_basecost * 1.15^(click_auto_4_rank+i))
end
end

-- if a second has passed
  if (qt >= 1) then
    -- checks to see if first upgrade has been purchased
    if (click_auto_1_running == true) then
      cases = cases + (click_auto_1_rank * click_auto_1_increase)
    end
    if (click_auto_2_running == true) then
      cases = cases + (click_auto_2_rank * click_auto_2_increase)
    end
    if (click_auto_3_running == true) then
      cases = cases + (click_auto_3_rank * click_auto_3_increase)
    end
    if (click_auto_4_running == true) then
      cases = cases + (click_auto_4_rank * click_auto_4_increase)
    end
    qt = 0
  end
end

function love.mousepressed(x,y,button,istouch)
  if button == 1 then
    --If hovering over Solve button
    if  x > 100 and x < 250 and y > 100 and y < 200 then 
    cases = cases + 1
  end
  -- If hovering over "Buy 1"
  if  x > 120 and x < 220 and y > 300 and y < 350 then 
    currently_buying = 1
  end
  
    if  x > 120 and x < 220 and y > 360 and y < 410 then 
    currently_buying = 10
  end

  --If hovering over "Cursor"
    if  x > 300 and x < 500 and y > 100 and y < 200 then 
      --Checks to see if the player can afford the upgrade
      if (cases -(click_auto_1_cost) >= 0) then
        cases = cases - (click_auto_1_cost)
        click_auto_1_rank = click_auto_1_rank + currently_buying
        --enables the auto-clicks for this upgrade
        if (click_auto_1_running == false) then
          click_auto_1_running = true
        end
      end
    end
    
    --If hovering over "Junior Detective"
    if  x > 300 and x < 500 and y > 210 and y < 310 then 
      --Checks to see if the player can afford the upgrade
      if (cases -(click_auto_2_cost) >= 0) then
        cases = cases - (click_auto_2_cost)
        click_auto_2_rank = click_auto_2_rank + currently_buying
        --enables the auto-clicks for this upgrade
        if (click_auto_2_running == false) then
          click_auto_2_running = true
        end
      end
    end
    
    --If hovering over "Senior Detective"
    if  x > 300 and x < 500 and y > 320 and y < 420 then 
      --Checks to see if the player can afford the upgrade
      if (cases -(click_auto_3_cost) >= 0) then
        cases = cases - (click_auto_3_cost)
        click_auto_3_rank = click_auto_3_rank + currently_buying
        --enables the auto-clicks for this upgrade
        if (click_auto_3_running == false) then
          click_auto_3_running = true
        end
      end
    end
    
        --If hovering over "DCI"
    if  x > 300 and x < 500 and y > 430 and y < 530 then 
      --Checks to see if the player can afford the upgrade
      if (cases -(click_auto_4_cost) >= 0) then
        cases = cases - (click_auto_4_cost)
        click_auto_4_rank = click_auto_4_rank + currently_buying
        --enables the auto-clicks for this upgrade
        if (click_auto_4_running == false) then
          click_auto_4_running = true
        end
      end
    end
    
  end
  if button == 2 then
      if  x > 100 and x < 250 and y > 100 and y < 200 then 
    cases = cases + 100
  end
  end

end

--[[
function caseClick(x,y,button,cases)
   if love.mouse.isDown(1) then
    if button == 1 and x > 100 and x < 250 and y > 100 and y < 200 then 
      cases = cases + 1
     end
    end
end
 ]]--

function love.draw()
	-- let's draw a background
	love.graphics.setColor(255,255,255,255)

	love.graphics.draw(bg)

--Draws clickable buttons
  love.graphics.draw(click, 100, 100)
  love.graphics.draw(clicktext, 125, 190)
  love.graphics.draw(click_auto_1, 300, 100)
  love.graphics.draw(click_auto_2, 300, 210)
  love.graphics.draw(click_auto_3, 300, 320)
  love.graphics.draw(click_auto_4, 300, 430)
  love.graphics.setColor(105,105,105,255)
  love.graphics.rectangle( "fill", 90, 45, 180, 50 )
--Beta text
  love.graphics.print(x,0,0)
  love.graphics.print(y,30,0)

--Cases solved text
	love.graphics.setColor(255,255,255,255)
  love.graphics.print("Cases Solved",100,50)
  love.graphics.print("Total CPS",100,70)
  love.graphics.print(cases, 200,50)
  
  --Draw Buy count buttons
  if (currently_buying == 1) then
    love.graphics.draw(buy_1_lit, 120,300)
  else
    love.graphics.draw(buy_1, 120,300)
  end

  if (currently_buying == 10) then
    love.graphics.draw(buy_10_lit, 120,360)
  else
    love.graphics.draw(buy_10, 120,360)
  end
  

  love.graphics.print(((click_auto_1_rank*1) +(click_auto_2_rank*5) +(click_auto_3_rank*10) + (click_auto_4_rank*25)), 200,70)

--Auto clicker 1
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.print("Rank: ",310,150)
  love.graphics.print(click_auto_1_rank,350,150)

  love.graphics.print("Cost: ",390,150)
  love.graphics.print(click_auto_1_cost,425,150)
  
  love.graphics.print("Cases per second: ",310,170)
  love.graphics.print(click_auto_1_rank * click_auto_1_increase,430,170)
  
  --Auto clicker 2
  
      love.graphics.print("Rank: ",310,260)
  love.graphics.print(click_auto_2_rank,350,260)

  love.graphics.print("Cost: ",390,260)
  love.graphics.print(click_auto_2_cost,425,260)
  
      love.graphics.print("Cases per second: ",310,280)
  love.graphics.print((click_auto_2_rank* click_auto_2_increase),430,280)
  
    --Auto clicker 3
  
      love.graphics.print("Rank: ",310,370)
  love.graphics.print(click_auto_3_rank,350,370)

  love.graphics.print("Cost: ",390,370)
  love.graphics.print(click_auto_3_cost,425,370)
  
      love.graphics.print("Cases per second: ",310,390)
  love.graphics.print((click_auto_3_rank* click_auto_3_increase),430,390)
  
      --Auto clicker 4
  
      love.graphics.print("Rank: ",310,480)
  love.graphics.print(click_auto_4_rank,350,480)

  love.graphics.print("Cost: ",390,480)
  love.graphics.print(click_auto_4_cost,425,480)
  
      love.graphics.print("Cases per second: ",310,500)
  love.graphics.print((click_auto_4_rank* click_auto_4_increase),430,500)

  --love.graphics.print(package.path)

end

  
